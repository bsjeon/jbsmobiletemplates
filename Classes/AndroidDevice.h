//
//  AndroidDevice.h
//  JBSMobileTemplates
//
//  Created by Jeon ByungSoo on 13. 7. 14..
//
//

#ifndef __JBSMobileTemplates__AndroidDevice__
#define __JBSMobileTemplates__AndroidDevice__

#include "JBSPrefix.h"
#include "MobileDevice.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include <platform/android/jni/JniHelper.h>

extern "C++" {
    class AndroidDevice : public MobileDevice
    {
    public:
        AndroidDevice();
        virtual ~AndroidDevice();
        
        static AndroidDevice* m_device;
        static AndroidDevice* sharedDevice();
//        void fireLocalNotification(std::string message, int remainTime, std::string notificationKey);
        
        virtual void LogJBS();
    };
}

#endif
#endif /* defined(__JBSMobileTemplates__AndroidDevice__) */
