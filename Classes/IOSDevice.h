//
//  IOSDevice.h
//  JBSMobileTemplates
//
//  Created by Jeon ByungSoo on 13. 7. 14..
//
//

#ifndef __JBSMobileTemplates__IOSDevice__
#define __JBSMobileTemplates__IOSDevice__

#include "JBSPrefix.h"
#include "MobileDevice.h"

class IOSDevice : public MobileDevice {
private:

public:
    IOSDevice();
    virtual ~IOSDevice();
    
    static IOSDevice* m_device;
    static IOSDevice* sharedDevice();
    void fireLocalNotification(std::string message, int remainTime, std::string notificationKey);

    virtual void LogJBS();
};

#endif