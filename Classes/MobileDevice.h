//
//  MobileDevice.h
//  JBSMobileTemplates
//
//  Created by Jeon ByungSoo on 13. 7. 14..
//
//

#ifndef __JBSMobileTemplates__MobileDevice__
#define __JBSMobileTemplates__MobileDevice__

#include "JBSPrefix.h"

class MobileDevice
{
private:
    
public:
    MobileDevice();
    virtual ~MobileDevice();
    
    static MobileDevice* m_device;
    static MobileDevice* sharedDevice();
    virtual void LogJBS() = 0;
};


#endif /* defined(__JBSMobileTemplates__MobileDevice__) */
