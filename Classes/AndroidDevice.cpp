//
//  AndroidDevice.cpp
//  JBSMobileTemplates
//
//  Created by Jeon ByungSoo on 13. 7. 14..
//
//

#include "AndroidDevice.h"
#include <algorithm>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
extern "C++" {

    AndroidDevice* AndroidDevice::m_device;

    AndroidDevice::AndroidDevice()
    {
        
    }
    
    AndroidDevice::~AndroidDevice()
    {
        
    }
    
    AndroidDevice* AndroidDevice::sharedDevice()
    {
        if (m_device == NULL) {
            m_device = new AndroidDevice();
        }
        return m_device;
    }
    
    //void AndroidDevice::fireLocalNotification(std::string message, int remainTime, std::string notificationKey){
    //    CCLog("IOSDevice::fireLocalNotification()");
    //
    //    // Create UILocalNotification Object
    //    UILocalNotification *noti = [[UILocalNotification alloc]init];
    //
    //    noti.fireDate = [NSDate dateWithTimeIntervalSinceNow:remainTime];
    //
    //
    //    // timeZone Setup.
    //    noti.timeZone = [NSTimeZone systemTimeZone];
    //
    //    // Notification Message Setup
    //    noti.alertBody = [NSString stringWithUTF8String:message.c_str()];
    //
    //    // Notification Action Setup
    //    noti.alertAction = @"GOGO";
    //
    //    // IconBadgeNumber Setup
    //    noti.applicationIconBadgeNumber = 1;
    //
    //    // Notification Sound Setup. I can use homegrown sound (nil = no sound)
    //    noti.soundName = UILocalNotificationDefaultSoundName;
    //
    //    // Random User Info Setup (No appearing in notification msg)
    //    noti.userInfo = [NSDictionary dictionaryWithObject:[NSString stringWithUTF8String:notificationKey.c_str()] forKey:@"User Info"];
    //    
    //    // Register Notification by using UIApplication
    //    [[UIApplication sharedApplication] scheduleLocalNotification:noti];
    //}
    
    void AndroidDevice::LogJBS()
    {
        CCLog("JBS WOW!");
    }

}
#endif