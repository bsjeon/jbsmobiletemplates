//
//  MobileDevice.cpp
//  JBSMobileTemplates
//
//  Created by Jeon ByungSoo on 13. 7. 14..
//
//

#include "MobileDevice.h"
#include "IOSDevice.h"
#include "AndroidDevice.h"

MobileDevice::MobileDevice()
{
    
}

MobileDevice::~MobileDevice()
{
    
}

MobileDevice* MobileDevice::m_device;

MobileDevice* MobileDevice::sharedDevice()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    return IOSDevice::sharedDevice();
#else
    return AndroidDevice::sharedDevice();
#endif
}
